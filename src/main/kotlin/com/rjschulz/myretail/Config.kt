package com.rjschulz.myretail

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
class HttpClientConfig {

    @Bean
    fun okHttpClient(
            apiKeyIntercepter: ApiKeyInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(apiKeyIntercepter)
                .build()
    }

}

@Component
class ApiKeyInterceptor(
    @Value("\${api-key}") val apiKey: String
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain) : Response {
        val request = chain.request().newBuilder()
                .header("x-api-key", apiKey)
                .build()
        return chain.proceed(request)
    }

}