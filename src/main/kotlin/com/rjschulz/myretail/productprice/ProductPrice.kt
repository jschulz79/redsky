package com.rjschulz.myretail.productprice

import jakarta.validation.constraints.DecimalMin
import jakarta.validation.constraints.Digits
import org.slf4j.LoggerFactory
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*

@Document("productprice")
data class ProductPrice(

        @Id
        var id: String?,

        var productId: Long?,

        @field:DecimalMin(value = "0.0", inclusive = false)
        @field:Digits(integer = 8, fraction = 2)
        var productPrice: BigDecimal?

)

data class ProductPriceUpdateRequest(
        @field:DecimalMin(value = "0.0", inclusive = false)
        @field:Digits(integer = 8, fraction = 2)
        var productPrice: BigDecimal?
)


interface ProductPriceRepository : MongoRepository<ProductPrice?, String?> {
    fun findByProductId(productId: Long?): ProductPrice?
}


/**
 * The ProductPrice Service encapsulates the ProductPrice data obtained from a locally hosted database.
 * It could be split off as an external service if need be.
 */
@Component
class ProductPriceService(
        private val productPriceRepository: ProductPriceRepository
) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    fun findByProductId(productId: Long): ProductPrice? {
        return productPriceRepository.findByProductId(productId)
    }

    fun updatePriceForProduct(productRequest: ProductPrice): Boolean {
        return try {
            productPriceRepository.save(productRequest)
            true
        } catch (ex: Exception) {
            log.error("Exception during save of ProductPrice {}", productRequest)
            false
        }
    }
}
