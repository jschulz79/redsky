package com.rjschulz.myretail.product

import com.rjschulz.myretail.productinfo.ProductInfoService
import com.rjschulz.myretail.productprice.ProductPriceService
import com.rjschulz.myretail.productprice.ProductPriceUpdateRequest
import jakarta.validation.Valid
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import java.math.BigDecimal
import java.util.concurrent.Callable
import java.util.concurrent.Executors

/**
 * Main API for myRetail product data.
 * 2 data sources are composited together: 1 external API, 1 local database.
 * Retrieving data by productId and updating the product price are supported.
 */
@RestController
class ProductController(
        private val productPriceService: ProductPriceService,
        private val productInfoService: ProductInfoService
) {

    private val log = LoggerFactory.getLogger(this.javaClass)
    @GetMapping("/products/{id}")
    fun getProductWithPricing(@PathVariable(name = "id") id: Long): ResponseEntity<Product> {
        return try {

            // separate the API and DB calls into their own threads so they will run in parallel
            // in case one (or both) are slow.
            val executorService = Executors.newFixedThreadPool(2)
            val apiCallable = Callable {
                productInfoService.getProductById(id)
            }
            val dbCallable = Callable {
                productPriceService.findByProductId(id)
            }
            val apiFuture = executorService.submit(apiCallable)
            val dbFuture = executorService.submit(dbCallable)
            val productInfo = apiFuture.get()
            val productPriceFromDb = dbFuture.get()
            executorService.shutdown()

            var productPrice: BigDecimal? = productPriceFromDb?.productPrice
            val productWithPrice = Product(
                    productInfo.data.product.tcin,
                    productInfo.data.product.item.productDescription.title,
                    productPrice
            )
            ResponseEntity.ok(productWithPrice)

        } catch (ex: Exception) {
            log.error("There was an exception attemtping to get data from services {}", ex.message)
            if (ex.cause is HttpClientErrorException) {
                ResponseEntity.notFound().build()
            } else {
                ResponseEntity.internalServerError().build()
            }
        }
    }

    @PutMapping("/products/{id}")
    fun setProductPrice(@PathVariable(name = "id") id: Long,
                        @Valid @RequestBody productRequest: ProductPriceUpdateRequest
    ): ResponseEntity<String> {

        productPriceService.findByProductId(id)?.let {
            it.productPrice = productRequest.productPrice
            if (productPriceService.updatePriceForProduct(it)) {
                return ResponseEntity.ok("Price updated for product: $id")
            } else {
                return ResponseEntity.badRequest().body("Product Price was not updated")
            }
        } ?: run {
            return ResponseEntity.notFound().build()
        }
    }
}

data class Product(
        val productId: Long,
        val name: String,
        val price: BigDecimal?
)
