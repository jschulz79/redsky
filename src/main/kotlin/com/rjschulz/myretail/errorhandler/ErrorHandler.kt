package com.rjschulz.myretail.errorhandler

import com.fasterxml.jackson.annotation.JsonFormat
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.NoHandlerFoundException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime
import java.util.*
import java.util.function.Consumer


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class ErrorHandler : ResponseEntityExceptionHandler() {

    private val log = LoggerFactory.getLogger(this.javaClass)
    override fun handleMethodArgumentNotValid(
            ex: MethodArgumentNotValidException,
            headers: HttpHeaders,
            status: HttpStatusCode,
            request: WebRequest
    ): ResponseEntity<Any> {

        val apiError = ApiError(HttpStatus.BAD_REQUEST)
        apiError.message = "Validation error"
        apiError.addValidationErrors(ex.bindingResult.fieldErrors)
        log.error("Invalid data received from request {}", request)
        return buildResponseEntity(apiError)

    }

    override fun handleNoHandlerFoundException(
            ex: NoHandlerFoundException,
            headers: HttpHeaders,
            status: HttpStatusCode,
            request: WebRequest
    ): ResponseEntity<Any> {

        val apiError = ApiError(HttpStatus.BAD_REQUEST)
        apiError.message = String.format("Could not find the %s method for URL %s", ex.httpMethod, ex.requestURL)
        apiError.debugMessage = ex.message
        return buildResponseEntity(apiError)

    }

    private fun buildResponseEntity(apiError: ApiError): ResponseEntity<Any> {
        return ResponseEntity(apiError, apiError.status)
    }
}

/**
 * class to encapsulate API errors for friendlier error messaging
 */
class ApiError (val status: HttpStatus) {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    var timestamp: LocalDateTime = LocalDateTime.now()
    var message: String? = null
    var debugMessage: String? = null
    private var validationErrors: MutableList<ApiValidationError> = mutableListOf()

    fun getValidationErrors(): List<ApiValidationError>? {
        return validationErrors
    }

    fun addValidationErrors(fieldErrors: List<FieldError>) {
        fieldErrors.forEach(Consumer {
            fieldError: FieldError -> this.addValidationError(fieldError)
        })
    }

    private fun addValidationError(fieldError: FieldError) {
        this.addValidationError(
                fieldError.objectName,
                fieldError.field,
                fieldError.rejectedValue,
                fieldError.defaultMessage)
    }

    private fun addValidationError(`object`: String, field: String, rejectedValue: Any, message: String) {
        addValidationError(ApiValidationError(`object`, field, rejectedValue, message))
    }

    private fun addValidationError(validationError: ApiValidationError) {
        validationErrors.add(validationError)
    }
}

/**
 * class to encapsulate API validation errors due to invalid data
 * @see javax.validation.Valid
 */
data class ApiValidationError (
    var `object`: Any? = null,
    var field: String? = null,
    var rejectedValue: Any? = null,
    var message: String? = null
)

