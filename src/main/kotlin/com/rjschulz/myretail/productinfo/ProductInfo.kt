package com.rjschulz.myretail.productinfo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException
import java.io.IOException


/**
 * The ProductInfo Service encapsulates the ProductInfo data obtained from an external API.
 * It could be split off as an external service if need be.
 */
@Component
class ProductInfoService(private val productInfoClient: ProductInfoClient) {
    fun getProductById(productId: Long): ProductInfo {
        return productInfoClient.getProductById(productId)
    }
}


@Component
class ProductInfoClient(
        @param:Value("\${com.rjschulz.redsky.productInfo.api}")
        private val productApiUrl: String,
        private val okHttpClient: OkHttpClient
) {

    private val log = LoggerFactory.getLogger(this.javaClass)
    fun getProductById(productId: Long): ProductInfo {
        val call = okHttpClient.newCall(
                Request.Builder()
                        .url(
                                productApiUrl.toHttpUrl().newBuilder()
                                        .addQueryParameter("tcin", productId.toString())
                                        .build()
                        )
                        .build()
        )
        try {
            val response = call.execute()
            if (response.isSuccessful) {
                try {
                    return jacksonObjectMapper().readValue(response.body?.string(), ProductInfo::class.java)
                } catch (ex: IOException) {
                    log.error("There was an error mapping the response data {}", ex.message)
                }
            } else if (response.code == 404) {
                throw HttpClientErrorException(HttpStatus.NOT_FOUND)
            }
            throw RuntimeException("Something bad happened trying to fetch data from external API")
        } catch (ex: IOException) {
            throw RuntimeException(ex)
        }
    }
}


@JsonIgnoreProperties(ignoreUnknown = true)
data class ProductInfo(
        var data: ProductData
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProductData(
        val product: Product
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Product(
        val tcin: Long,
        val item: Item,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Item(
        @JsonProperty("product_description")
        val productDescription: ProductDescription
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProductDescription(
        val title: String
)

