package com.rjschulz.myretail

import com.rjschulz.myretail.product.ProductController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class LoadContextSpec extends Specification {

    @Autowired (required = false)
    private ProductController productController

    def "when context is loaded then all expected beans are created"() {
        expect: "the controller is created"
        productController
    }
}
