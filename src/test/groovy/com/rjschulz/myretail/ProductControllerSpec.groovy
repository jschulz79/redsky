package com.rjschulz.myretail

import com.rjschulz.myretail.product.ProductController
import com.rjschulz.myretail.productinfo.Item
import com.rjschulz.myretail.productinfo.Product
import com.rjschulz.myretail.productinfo.ProductData
import com.rjschulz.myretail.productinfo.ProductDescription
import com.rjschulz.myretail.productinfo.ProductInfo
import com.rjschulz.myretail.productinfo.ProductInfoService
import com.rjschulz.myretail.productprice.ProductPrice
import com.rjschulz.myretail.productprice.ProductPriceService
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification
import org.springframework.test.web.servlet.request.*

@AutoConfigureMockMvc
@WebMvcTest(controllers = [ProductController])
class ProductControllerSpec extends Specification {

    @Autowired
    private MockMvc mvc

    @SpringBean
    ProductPriceService productPriceService = Mock()

    @SpringBean
    ProductInfoService productInfoService = Mock()

    def "when get is performed then the response has status 200 and content is the product info with price"() {

        given:
        productInfoService.getProductById(13860428L) >> {
            new ProductInfo(
                    new ProductData(
                            new Product(
                                    13860428L,
                                    new Item(
                                            new ProductDescription("whatever")
                                    ))))
        }
        productPriceService.findByProductId(13860428L) >> {
            new ProductPrice("121212", 13860428L, new BigDecimal("44.55"))
        }

        expect:
        mvc.perform(MockMvcRequestBuilders.get("/products/13860428"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .response
                .contentAsString.contains("13860428")
    }

    def "when put is performed with invalid request returns status 400 and contains a validation error"() {

        given:
        productPriceService.findByProductId(13860428L) >> {
            new ProductPrice("213213213", 13860428L, new BigDecimal("44.55"))
        }

        expect:
        mvc.perform(MockMvcRequestBuilders.put("/products/13860428")
                .content(  "{\"productPrice\": \"32.964\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn()
                .response
                .contentAsString.contains("validationErrors")
    }

    def "when put is performed with valid request returns status 200"() {

        given:
        productPriceService.findByProductId(13860428L) >> {
            new ProductPrice("312321231", 13860428L, new BigDecimal("44.55"))
        }
        productPriceService.updatePriceForProduct(_ as ProductPrice) >> true

        expect:
        mvc.perform(MockMvcRequestBuilders.put("/products/13860428")
                .content(  "{\"productPrice\": \"32.94\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .response
                .contentAsString.contains("updated")
    }

}
