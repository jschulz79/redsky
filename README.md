# myRetail Product API

### Description
The myRetail Product API is a RESTful web service providing product info and pricing data.

### Getting Started
The project is a SpringBoot project built using Gradle.
It requires a running local MongoDB database, if using the default profile.
If using the "cloud" profile, it will connect to MongoDB running in Atlas.
The API will be running on port 8080 by default.

There are a few ways to run the project.

**1. Use gradle spring boot plugin:** 
    
    ./gradlew bootRun

**2. Build the project with gradle and run via java:**
    
    ./gradlew clean build
    
    java -jar build/libs/myretail-0.0.1-SNAPSHOT.jar
    
**3. Use the spring profile "cloud" to connect to MongoDB running in Atlas**
    
    set MONGO_DB_USER and MONGO_DB_PWD environment variables
    
    ./gradlew clean build
   
    java -jar -Dspring.profiles.active=cloud build/libs/myretail-0.0.1-SNAPSHOT.jar

### REST documentation
A single endpoint is provided supporting GET and PUT requests.

**GET /products/{productId}**

Retrieves the Product data including productId, name, and price.

productId is a number.

**PUT /products/{productId}**

Updates the price of a product for the given productId.

productId is a number.

Sample JSON request body: **{"productPrice": "32.99"}**

